import React, { Component } from "react";
import './App.css';



//change github data to your stuff
const GITHUB_USER_INFO = "https://api.github.com/users/ZellMarsh";
class App extends Component {
  state = {
    user: {},
    active: false
  };

  handleToggle = event => {
    console.log("button clicked");
    if (this.state.active === true) {
      this.setState({ active: false });
    } else {
      fetch(GITHUB_USER_INFO)
        .then(res => res.json())
        .then(user => {
          this.setState({ user, active: true });
        });
    }
  };

  render() {
    return ( 
      <React.Fragment>
        <button onClick={this.handleToggle}>Toggle</button>
        {this.state.active === true && (
          <card>
            <img src={this.state.user.avatar_url} alt='avatar' />
            <h2>{this.state.user.name}</h2>
            <p>{this.state.user.bio}</p>        
            <p>{this.state.user.followers}</p>
           
          </card>
        )}
      </React.Fragment>
    );
  }
}

export default App;
